/**
 *  
 *  Based on original project, ispiration, cut'n'paste pattern... 
 *  openutils-mgnlutils http://www.openmindlab.com/lab/products/mgnlutils.html 
 *  File: it.openutils.mgnlutils.el.MgnlUtilsElFunctions
 *  
 */

package it.strap.magnolia.el;

import info.magnolia.context.MgnlContext;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Utils elFunction for Module.
 * @author cstrap
 * @version $Id: $
 */
public final class Xhtml2PdfElFunctions
{

    /**
     * Logger.
     */
    private static Logger log = LoggerFactory.getLogger(Xhtml2PdfElFunctions.class);

    public Xhtml2PdfElFunctions()
    {
        // allow instantion for freemarker
    }

    /**
     * Returns the base URL for the request (scheme + server + port + context path, like http://server:81/context/).
     * @return the base URL for the request (scheme + server + port + context path, like http://server:81/context/
     */
    public static String baseUrl()
    {
        HttpServletRequest request = MgnlContext.getWebContext().getRequest();

        StringBuffer baseUrl = new StringBuffer();
        baseUrl.append(request.getScheme());
        baseUrl.append("://");
        baseUrl.append(request.getServerName());

        if (("http".equals(request.getScheme()) && request.getServerPort() != 80)
            || ("https".equals(request.getScheme()) && request.getServerPort() != 443))
        {
            baseUrl.append(":");
            baseUrl.append(request.getServerPort());
        }

        log.debug(String.format("%s%s", baseUrl.toString(), request.getContextPath()));

        return String.format("%s%s", baseUrl.toString(), request.getContextPath());
    }

}
