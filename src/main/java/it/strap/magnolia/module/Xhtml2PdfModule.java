package it.strap.magnolia.module;

import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Module Lifecylce, log start and stop.
 * @author cstrap
 * @version $Id: $
 */
public class Xhtml2PdfModule implements ModuleLifecycle
{

    /**
     * Logger.
     */
    private Logger log = LoggerFactory.getLogger(Xhtml2PdfModule.class);

    /**
     * {@inheritDoc}
     */
    public void start(ModuleLifecycleContext moduleLifecycleContext)
    {
        log.info("mgnl-xhtml2pdf module: start");
    }

    /**
     * {@inheritDoc}
     */
    public void stop(ModuleLifecycleContext moduleLifecycleContext)
    {
        log.info("mgnl-xhtml2pdf module: stop");
    }

}
