package it.strap.magnolia.servlets;

import info.magnolia.cms.core.Content;
import info.magnolia.cms.core.SystemProperty;
import info.magnolia.context.MgnlContext;
import it.strap.magnolia.el.Xhtml2PdfElFunctions;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;


/**
 * Servlet implementation class Html2Pdf with groovy script.
 */
public class Xhtml2Pdf extends HttpServlet
{

    private static final long serialVersionUID = 1L;

    /**
     * Logger.
     */
    private Logger log = LoggerFactory.getLogger(Xhtml2Pdf.class);

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
        IOException
    {

        long start = System.currentTimeMillis();
        try
        {
            String pdfResponseTitle = "download";

            Content content = MgnlContext.getAggregationState().getCurrentContent();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            if (content != null)
            {
                // A regex for parsing special chars to '_'
                pdfResponseTitle = content
                    .getTitle()
                    .replaceAll("\\s|[\\\\+\\-\\!\\(\\)\\:\\^\\]\\{\\}\\~\\*\\?\"\\[\\]|]|[$,/,']", "_")
                    .toLowerCase();

                response.setHeader("Expires", "0");
                response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                response.setHeader("Pragma", "public");
                response.setContentType("application/pdf");
                response.addHeader(
                    "Content-Disposition",
                    "attachment; filename=" + String.format("%s.pdf", pdfResponseTitle));

                String uri = String.format("%s%s.xhtml2pdf", Xhtml2PdfElFunctions.baseUrl(), content.getHandle());

                log.info("Author Instance: " + SystemProperty.getBooleanProperty("magnolia.bootstrap.authorInstance"));

                if (SystemProperty.getBooleanProperty("magnolia.bootstrap.authorInstance"))
                {
                    uri = String.format(
                        "%s?mgnlUserId=%s&mgnlUserPSWD=%s",
                        uri,
                        MgnlContext.getUser().getName(),
                        MgnlContext.getUser().getPassword());
                }

                log.info("URI: " + uri);

                URL url = new URL(uri);

                InputStream inputStream = url.openStream();

                ByteArrayOutputStream xhtmlOutputStream = new ByteArrayOutputStream();
                byte buf[] = new byte[1024];
                int len;
                while ((len = inputStream.read(buf)) > 0)
                {
                    xhtmlOutputStream.write(buf, 0, len);
                }

                DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

                ByteArrayInputStream xhtmlInputStream = new ByteArrayInputStream(xhtmlOutputStream.toByteArray());

                Document xhtmlContent = documentBuilder.parse(xhtmlInputStream);

                ITextRenderer renderer = new ITextRenderer();

                renderer.setDocument(xhtmlContent, null);
                renderer.layout();
                renderer.createPDF(baos);

                ServletOutputStream servletOutputStream = response.getOutputStream();
                baos.writeTo(servletOutputStream);
                servletOutputStream.flush();

                // Close all stream
                servletOutputStream.close();
                baos.close();
                inputStream.close();
                xhtmlInputStream.close();
                xhtmlOutputStream.close();
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            log.debug("Time to gen: {} ms", (System.currentTimeMillis() - start));

        }
    }
    /**
     * @return url
     */

}