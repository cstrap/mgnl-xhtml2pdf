<jsp:root version="2.0" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:c="http://java.sun.com/jsp/jstl/core" 
     xmlns:cms="urn:jsptld:cms-taglib" xmlns:xpdf="mgnlxhtml2pdf">
     
    <jsp:directive.page contentType="text/html; charset=UTF-8" session="false" />
    
    <!-- Rewrite the baseUrl for DTD, otherwise the parser is slow when parsing xhtml -->
    <!-- Make sure that all the static elements in your page have a call to baseUrl() -->
    <c:set var="appCtx" value="${xpdf:baseUrl()}" scope="request" />
    <c:set var="docroot" value="${appCtx}/docroot/mgnl-xhtml2pdf" scope="request" />

    <jsp:text>
        <![CDATA[ <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "${docroot}/DTD/xhtml1-strict.dtd">]]>
    </jsp:text>

    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <cms:links />
            <title>Test xhtml2pdf</title>
        </head>
        <body>
           <cms:mainBar /> 
           <h1>${actpage.title}</h1>
           <img src="${docroot}/img/logo.jpg" alt="Logo" />
           <br />
           <a href="${xpdf:baseUrl()}${actpage.handle}.pdf">Get PDF!</a>
        </body>
    </html>
</jsp:root>
